package netflix;

public class Contenuto {
/**
 * possibili stati in cui pu� trovarsi il <code>Contenuto</code>
 */
	private String[] statif={"fermo","in riproduzione","finito", "stoppato"};
	private String statof;
	//public long min;
	protected long durata;
	protected long id;
	protected String titolo;
	protected String regista;
    protected long min=0;
    
    public void setmin(long min) {
    	this.min=min;
    }
	
    public long getmin() {
    	return this.min;
    }
	
    
	public String toString() {
		return " [id=" + id + ", titolo=" + titolo
				+ ", regista=" + regista + ", durata=" + durata + ", minuto=" + min + " ]";
	}
	
	/** Costruttore di Contenuto
	 * <p>
	 * stampa messaggio a video
	 * <p>
	 * setta lo stato in fermo
	 * <p>
	 * inizializza la durata del Contenuto
	 * @param id identificativo del contenuto
	 * @param regista regista del contenuto
	 * @param durata durata del contenuto
	 * @param min minuto cui � stato eventualmente interrotto il film
	 */
	Contenuto(long id, String titolo, String regista, long durata, long min){
		//System.out.println("Entity Contenuto istanziata");
		statof=statif[0];
	//	min=0;
		this.id = id;
		this.titolo = titolo;
		this.regista = regista;
		this.durata = durata;
		this.min=min;
		
		linguacurr=Lingua.italiano;
		linguasotto=Sottotitoli.disattivati;
	}
	
	/**restituisce durata del contenuto
	 * 
	 * @return long durata del contenuto
	*/
	public long getdur() {
		return durata;
	}
	
	/** Preleva le scene del <code>Contenuto</code>
	 * <p>
	 *stampa messaggio a video
	 *<p>
	 *setta lo stato del <code>Contenuto</code> "in riproduzione"
	 * <p>
	 * invocato da metodo {@link netflix.PaginaVisualizzazione#avvia(Contenuto, Cronometro)}
	@see  netflix.Contenuto#statif
	
	 */
	public void preleva() {
		System.out.println("Il contenuto viene prelevato e viene mostrato da PaginaVisualizzazione");
	    statof=statif[1];
	    System.out.println("Lo stato del film � "+ getstatof());
	}

	/** setta lo stato del contenuto in riproduzione
	 * <p>
	 * invocato da  metodo <code>avvia()</code>
	 * @return la string corrispondente allo stato del contenuto
	 @see netflix.PaginaVisualizzazione#avvia(Contenuto, Cronometro)
	*/
	public String getstatof() {
		return statof;
	}
	/** invocato da metodo <code>pausa()</code>
	 * <p>
	 * stampa messaggio a video
	 * <p>
	 * setta la stato del contenuto in "stoppato"
	 *  
	 *   @see netflix.PaginaVisualizzazione#pausa(Contenuto, Cronometro)
	
	*/
	public void ferma() {
		System.out.println("Il contenuto viene fermato");
		statof=statif[3];
		System.out.println("Lo stato del film � "+ getstatof());
	}
	
	
	/** invocato da metodo <code>fine()</code>
	 * <p>
	 * stampa messaggio a video
	 * <p>
	 * setta la stato del contenuto in "finito"
	 @see netflix.PaginaVisualizzazione#fine(Contenuto)
	*/
	public void finef() {
		statof=statif[2];
		System.out.println("Lo stato del film � "+ getstatof());
		
	}
	
	/*
   public void skip(long m) {
	   System.out.println("Contenuto portato avanti di "+m+" minuti");
   min=min+m;
   }
   public void back(long m) {
	   System.out.println("Contenuto portato avanti di "+m+" minuti");
   min=min-m;
   }

   public void inc() {
	   min=min+1;
   }
   public long getmin() {
	   return min;
   }
   */
	public  long getId() {
		return id;
	}
	
	public  String getTitolo() {
		return titolo;
	}
	
	public String getRegista() {
		return regista;
	}
		
   
   /** insieme di tutte le lingue possibili
	*/
   public enum Lingua {
	   italiano,
	   inglese,
	   francese,
	   spagnolo,
	   tedesco,
	   portoghese,
	   russo,
	   cinese;
   }
   
   /** insieme di tutti sottotitoli possibili
  	*/
   public enum Sottotitoli {
	   italiano,
	   inglese,
	   francese,
	   spagnolo,
	   tedesco,
	   portoghese,
	   russo,
	   cinese,
	   disattivati;
   }

private Lingua linguacurr;
private Sottotitoli linguasotto;

/** setta lingua 
 * <p>
 * invocato da cambialingua
 * @param  lingua da impostare
 * @see Lingua
 
*/
public void setlingua(String lingua) {
    this.linguacurr = Lingua.valueOf(lingua);
}

/** mostra lingua corrente
	*/
public void getlingua() {
	System.out.println("La lingua attuale � "+linguacurr);
}

/** setta sottotitoli 
 * <p>
 * invocato da cambiasotto
 * @param  lingua sottititolo da impostare
 * @see Sottotitoli
*/
public void setsotto(String lingua) {
    this.linguasotto = Sottotitoli.valueOf(lingua);
}

/** mostra sottotitoli correnti
*/
public void getsotto() {
	if (this.linguasotto==Sottotitoli.disattivati) {
		System.out.println("Sottotitoli disattivati");
	}
	else
	System.out.println("La lingua attuale dei sottotitoli � "+linguasotto);
}


   /** stampa a video gli elementi dell'enumerazione Lingua
    
    */
public static void enumIterate() {
    int i=1;
	for ( Lingua  li: Lingua.values()) {
        System.out.println(i+" "+li.name());
        i=i+1;
    }
}

/** stampa a video gli elementi dell'enumerazione Sottotitoli

 */
public static void enumIterateS() {
    int i=1;
	for ( Sottotitoli  li: Sottotitoli.values()) {
        System.out.println(i+" "+li.name());
        i=i+1;
    }
	
	
}
}