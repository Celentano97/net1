package netflix;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class VisualizzaCatalogoSerie {
	
	//Delimiter used in CSV file
	private static final String COMMA_DELIMITER = ";";
	
	//Indici degli attributi del contenuto 
	private static final int CONTENUTI_ID_IDX       = 0;
	private static final int CONTENUTI_TITOLO_IDX   = 1;
	private static final int CONTENUTI_AUTORE_IDX   = 2;
	private static final int CONTENUTI_DURATA_IDX   = 3; 
	private static final int CONTENUTI_STAGIONE_IDX = 5;
	private static final int CONTENUTI_PUNTATA_IDX  = 6;
	private static final int CONTENUTI_MIN_IDX  = 4;
	private static ArrayList<Serie>  contenutiFS;
	
	public static  ArrayList<Serie> arr (){
		return contenutiFS;
	}
	
	/**Crea il Catalogo delle <code>Serie</code>
	 * <p>
	 * Usato per mostrare il Catalogo all'utente
	 * @see netflix.VisualizzaCatalogo#VisualizzaCatalogo()
	 * @see netflix.Serie#Serie(long, String, String, long,long,long, int, int)
	 * @param fileName nome del file in cui � salvato il Catalogo dei Film
	 */
	public static void VisualizzaCatalogoSerie (String fileName) {

		BufferedReader fileReader = null;
		 	
        try {
        	
        	//Crea una nuova lista di contenuto che deve essere inserita in un file CSV 
        	contenutiFS = new ArrayList<Serie>();
        	
            String line = "";
            
            //Creazione di un File Reader
            fileReader = new BufferedReader(new FileReader(fileName));
            
            //Read the CSV file header to skip it
           //fileReader.readLine();
            
            //Read the file line by line starting from the second line
            while ((line = fileReader.readLine()) != null) {
                //Get all tokens available in line
                String[] tokens = line.split(COMMA_DELIMITER);
                
                if (tokens.length > 0) {
                	//Creazione di un nuovo oggetto contenuto e aggiunta degli attributi
                	Serie contenuti = new Serie(Long.parseLong(tokens[CONTENUTI_ID_IDX]), tokens[CONTENUTI_TITOLO_IDX], tokens[CONTENUTI_AUTORE_IDX], Long.parseLong(tokens[CONTENUTI_DURATA_IDX]), Long.parseLong(tokens[CONTENUTI_MIN_IDX]),Integer.parseInt (tokens[CONTENUTI_STAGIONE_IDX]),Integer.parseInt (tokens[CONTENUTI_PUNTATA_IDX]));
					contenutiFS.add(contenuti);
				}
            }
            
            //Stampa della lista dei contenuti          
            System.out.println("\nCATALOGO SERIE\n");
            
            for (Serie contenuti : contenutiFS) {
				System.out.println(contenuti.toString());
			}
        	} 
        	catch (Exception e) {
        	System.out.println("Errore nel CSV!!!");
            e.printStackTrace();
        	} finally {
            try {
                fileReader.close();
            } catch (IOException e) {
            	System.out.println("Errore mentre il CSV veniva chiuso!!!");
                e.printStackTrace();
            }
        }

	}

}