package netflix;

import netflix.Contenuto;
import netflix.PaginaVisualizzazione;

public class Cronometro{
	
	private long start=0;
	//private long stop=0;
	private boolean running=false;
	private long cont=0;
	private long av=0;
	private long i=0;
	
	/** fa partire il cronometro
	 */
	public void startf() {
		
		start=System.currentTimeMillis();
		running=true;
		
	}
	
	/** stoppa il cronometro
	 <p>
	 <code>cont</code> conteggia il tempo dallo start allo stop
	 
	 */
	public void stopf() {
		cont+=System.currentTimeMillis()- start;
		running=false;
		
	}
	
	/** conteggia il totale dei secondi portati avanti
	 * 
	 * @param a dice quanto � stato avanzato
	 */
   public void ava(long a) {
	   av+=a*1000;
	  
   }
   /** conteggia il totale dei secondi portati indietro
	 * 
	 * @param b dice quanto � stato indietreggiato
	 */
   public void ind(long b) {
	   
	   i+=(b*1000);
	   
   }
   
   /** controlla se il contenuto � terminato
    * <p>
    * invoca la chiusura della pagina e l'aggiornamento della cronologia se il tempo trascorso � maggiore della durata del film
    *<p>
    * e in seguito chiude l'applicazione
    * @param pag istanza della corrente PaginaVisualizzazione
    * @param c Contenuto in riproduzione
    *  @see netflix.PaginaVisualizzazione#fine(Contenuto)
    *   @see netflix.PaginaVisualizzazione#aggiorna()
    */
   
   public void control(PaginaVisualizzazione pag, Contenuto c) {
	   if(getime()>c.getdur()) {
		   pag.fine(c);
		   pag.aggiorna();
		   System.exit(0);
	   }
   }
	/** dice a che istante del contenuto siamo
	 * 
	 * @return il tempo corrente del contenuto
	 *
	 */
   //public long t=0; 
	public long getime() {
		long t;
		if(running) {
			t=(System.currentTimeMillis()-start +min + cont+av-i);
		}
		else {
			t=cont+av -i+min ;
		}
		return t/1000;
	}
	
	long min=0;
	/**Setta il minuto di interruzione
	 * 
	 * @param min minuto cui stato interrotto il contenuto
	 */
	public void setime(long min) {
		this.min=min*1000;
	}
	
}