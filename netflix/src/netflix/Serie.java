package netflix;

/**<code>Serie</code> classe derivata di <code>Contenuto</code>
 *
 */
public class Serie extends Contenuto {
	
	protected int puntata;
	protected int stagione;
	
	public String toString() {
		return " [id=" + id + ", titolo=" + titolo
				+ ", regista=" + regista + ", durata=" + durata + ", stagione =" + stagione+ ", puntata = " + puntata +",minuto= "+ min + "]";
	}
	
	/** costruttore di <code>Serie</code>
	 * @param id identificativo della Serie 
	 * @param titolo titolo della Serie 
	 * @param regista regista della Serie
	 * @param durata durata della puntata della Serie che viene riprodotta
	 * @param min minuto cui eventualmente � stata interrotta
	 * @param puntata puntata che viene riprodotta
	 * @param stagione stagione a cui appartiene la puntata riprodotta
	 */
	public Serie(long id, String titolo, String regista, long durata,long min, int stagione, int puntata) {
		super(id,titolo,regista,durata,min);
		//System.out.println("Entity Serie istanziata");
		this.puntata=puntata;
		this.stagione=stagione;
	}
	
	/** invocato quando si vuole sapere la puntata che si sta guardando
	 * 
	 * <p>
	 * stampa messaggio a video 
	 */
	public void getpuntata() {
		System.out.println("La puntata della serie � "+puntata);
	}
	
	/** invocato quando si vuole sapere la stagione a cui appartiene la puntata
	 *<p>
	 *stampa messaggio a video 
	 *
	 */
	public void getstagione() {
		System.out.println("La stagione della serie � "+stagione);
	}
	
	/**Fornisce il numero della puntata
	 * 
	 * @return puntata
	 */
	public int getpuntata1() {
		return puntata;
	}
	/**Fornisce il numero della stagione
	 * 
	 * @return stagione
	 */
	public int getstagione1() {
		return stagione;
	}
}