package netflix;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import netflix.Film.Genere;


public class VisualizzaCatalogoFilm {
	/**delimitatore usato nel file CSV per separare i campi
	 * 
	 */
	
	private static final String COMMA_DELIMITER = ";";
	/**indici degli attributi del contenuto
	 * 
	 */
	
	private static final int CONTENUTI_ID_IDX     = 0;
	private static final int CONTENUTI_TITOLO_IDX = 1;
	private static final int CONTENUTI_AUTORE_IDX = 2;
	private static final int CONTENUTI_DURATA_IDX = 3; 
	private static final int CONTENUTI_ANNO_IDX   = 5;
	private static final int CONTENUTI_GENERE_IDX = 6;
	private static final int CONTENUTI_MIN_IDX = 4; ///
	
	private static ArrayList<Film>  contenutiFS;
	
	
	/**
	 * 
	 * @return la lista dei Film
	 */
	public static  ArrayList<Film> arr (){
		return contenutiFS;
	}
	
	/**Crea il Catalogo dei <code>Film</code>
	 * <p>
	 * Usato per mostrare il Catalogo all'utente
	 * @see netflix.Film#Film(long, String, String, long, long, long, Genere)
	 * @see netflix.VisualizzaCatalogo#VisualizzaCatalogo()
	 * @param fileName nome del file in cui � salvato il Catalogo dei Film
	 */
	public static void VisualizzaCatalogoFilm (String fileName) {
   
	
	
	BufferedReader fileReader = null;
		
        try {
        	
        	//Crea una nuova lista di contenuto che deve essere inserita in un file CSV 
        	 contenutiFS = new ArrayList<Film>();
        	
            String line = "";
            
            //Creazione di un File Reader
            fileReader = new BufferedReader(new FileReader(fileName));
            
            //Read the CSV file header to skip it
           //fileReader.readLine();
            
            //Read the file line by line starting from the second line
            while ((line = fileReader.readLine()) != null) {
            	
                //Get all tokens available in line
                String[] tokens = line.split(COMMA_DELIMITER);
                
                if (tokens.length > 0) {                	
                	//Creazione di un nuovo oggetto contenuto e aggiunta degli attributi
                	Film contenuti = new Film(Long.parseLong(tokens[CONTENUTI_ID_IDX]), tokens[CONTENUTI_TITOLO_IDX], tokens[CONTENUTI_AUTORE_IDX], Long.parseLong (tokens[CONTENUTI_DURATA_IDX]), Long.parseLong (tokens[CONTENUTI_MIN_IDX]), Integer.parseInt (tokens[CONTENUTI_ANNO_IDX]),  Genere.valueOf(tokens[CONTENUTI_GENERE_IDX]));
					
                	contenutiFS.add(contenuti);
					System.out.println(contenuti.toString());
				}
            //}
           // System.out.println(Genere.valueOf(tokens[CONTENUTI_GENERE_IDX]));
            }
            
            //Stampa della lista dei contenuti            
            //  System.out.println("\nCATALOGO FILM\n");
            
           for (Film contenuti : contenutiFS) { 	
			//	System.out.println(contenuti.toString());
			//}
        } 
        }
        catch (Exception e) {
        	System.out.println("Errore nel CSV!!!");
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
            	System.out.println("Errore mentre il CSV veniva chiuso!!!");
                e.printStackTrace();
            }
        }

	}

}