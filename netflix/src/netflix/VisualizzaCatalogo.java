package netflix;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;
import java.io.*;
import netflix.VisualizzaCatalogoSerie;
import netflix.VisualizzaCatalogoFilm;

public class VisualizzaCatalogo {
	
	/**
	 * Mostra il Catalogo all'utente unendo il Catalogo dei Film e delle Serie
	 * @param fileName nome del file in cui viene salvato il Catalogo
	 * @exception IOException se il file da cui si vuole leggere il Catalogo non esiste
	 * @see netflix.VisualizzaCatalogoFilm#VisualizzaCatalogoFilm()
	 * @see netflix.VisualizzaCatalogoSerie#VisualizzaCatalogoSerie()
	 *  
	 */
	
	public static void VisualizzaCatalogo (String fileName) {

		try {
        PrintWriter pw = new PrintWriter("cat.csv"); 
         
        // BufferedReader object for file1.txt 
        BufferedReader br = new BufferedReader(new FileReader("cats.csv")); 
          
        String line = br.readLine();
        
        //copia di ogni linea di cats in cat
         
        System.out.println("Puoi vedere queste Serie: \n");
        while (line != null) 
        { 
            pw.println(line); 
            System.out.println(line+"\n");
            line = br.readLine(); 
        } 
          
        br = new BufferedReader(new FileReader("catf.csv")); 
          
        line = br.readLine(); 
        
        //copia di ogni linea di catf in cat
        
        System.out.println("Puoi vedere questi Film: \n ");
        while(line != null) 
        { 
            pw.println(line);
            System.out.println(line+"\n");
            line = br.readLine(); 
        } 
        
       
        pw.flush(); 
        
        
        // closing resources 
        br.close(); 
        pw.close(); 
        
		}catch(IOException e) {
			System.out.println("Errore nella scrittura del file o chiusura/flushing");
		}
 }
		
}