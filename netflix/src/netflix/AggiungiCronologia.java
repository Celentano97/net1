package netflix;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;


import netflix.Film.Genere;



public class AggiungiCronologia {
		
	//Delimiter used in CSV file
	private static final String COMMA_DELIMITER = ";";
	private static final String NEW_LINE_SEPARATOR = "\n";
	private static final int CONTENUTI_ID_IDX = 0;
	private static final int CONTENUTI_TITOLO_IDX = 1;
	private static final int CONTENUTI_AUTORE_IDX = 2;
	private static final int CONTENUTI_DURATA = 3; 
	private static final int CONTENUTI_MIN_IDX=4;
	//static Genere genere;
	private String filename;
	

	
	AggiungiCronologia(String filename) {
		this.filename=filename;
		
	}
	
	/**Aggiunge i contenuti visualizzati in cronologia
	 * 
	 * @param fileName file dove verr� inserito il contenuto
	 * @param c contenuto da aggiungiure
	 */
	public static void Aggiungi(String fileName, Contenuto c) {
		
			
		//Scanner scanner = new Scanner(System.in);

		    FileWriter fileWriter = null;
		    
		    Contenuto contenuto1=c;
		    ArrayList contenutiF = new ArrayList();
		    
		  // ArrayList<Contenuto> carica=new ArrayList<Contenuto>();
		    //carica=AggiungiCronologia.arrc();
		    
		     	contenutiF.add(contenuto1);
		          
			
			//inserisce nuovo contenuto alla cronologia
		    try {
		    	fileWriter = new FileWriter(fileName,true); 
						
				
                
				
				fileWriter.append(String.valueOf(contenuto1.getId()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto1.getTitolo()));
				fileWriter.append(COMMA_DELIMITER);					
				fileWriter.append(String.valueOf(contenuto1.getRegista()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto1.getdur()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto1.getmin()));
				fileWriter.append(COMMA_DELIMITER);
				//fileWriter.append(String.valueOf(contenuto1.getAnno()));
				//fileWriter.append(COMMA_DELIMITER);
				//fileWriter.append(String.valueOf(Film.getGen1()));
				
				
				fileWriter.append(NEW_LINE_SEPARATOR); //QUESTO
			
				System.out.println("Il nuovo contenuto � stato aggiunto con successo in cronologia !!!");
				System.out.println("E' stato aggiungo il contenuto " + contenuto1 + " alla cronologia");
				
			
				
				
			} catch (Exception e) {
				System.out.println("Errore nella scrittura del CSV !!!");
				e.printStackTrace();
			} finally {
				
				try {
					fileWriter.flush();
					fileWriter.close();
					
				} catch (IOException e) {
					System.out.println("Errore durante la chiusura/flushing della scrittura del file !!!");
	                e.printStackTrace();
				}				
			}
		    }
		
private static ArrayList<Contenuto>  contenuti;  //

/**
 * 
 * @return contenutii array dei contenuti presenti in cronologia
 */
	public static  ArrayList<Contenuto> arr (){
		return contenuti;
	}
	
	/**Mostra la cronologia dei contenuti visualizzati
	 * 
	 * @param fileName file in cui sono presenti i contenuti visti
	 */
	public static void VisualizzaCronologia (String fileName) {
		
		{

			BufferedReader fileReader = null;
	     
	        try {
	        	
	        	//Crea una nuova lista di contenuto che deve essere inserita in un file CSV 
	        	 contenuti = new ArrayList<Contenuto>();
	        	
	            String line = "";
	            
	            //Creazione di un File Reader
	            fileReader = new BufferedReader(new FileReader(fileName));
	            
	            //Read the CSV file header to skip it
	           //fileReader.readLine();
	            
	            //Read the file line by line starting from the second line
	            while ((line = fileReader.readLine()) != null) {
	                //Get all tokens available in line
 	                String[] tokens = line.split(COMMA_DELIMITER);
	                if (tokens.length > 0) {
	                	//Creazione di un nuovo oggetto contenuto e aggiunta degli attributi
	                	Contenuto contenutic = new Contenuto(Long.parseLong(tokens[CONTENUTI_ID_IDX]), tokens[CONTENUTI_TITOLO_IDX], tokens[CONTENUTI_AUTORE_IDX], Integer.parseInt (tokens[CONTENUTI_DURATA]), Long.parseLong (tokens[CONTENUTI_MIN_IDX]));
						contenuti.add(contenutic);
					}
	            }
	            
	            //Stampa della lista dei contenuti 
	            for (Contenuto contenutic : contenuti) {
					System.out.println(contenutic.toString());
				}
	            
	            System.out.println("Il numero di contenuti in cronologia � : "+ contenuti.size());
	        } 
	        catch (Exception e) {
	        	System.out.println("Errore nel CSV!!!");
	            e.printStackTrace();
	        } finally {
	            try {
	                fileReader.close();
	            } catch (IOException e) {
	            	System.out.println("Errore mentre il CSV veniva chiuso!!!");
	                e.printStackTrace();
	            }
	        }

		}
	  
	}
	
	
	}