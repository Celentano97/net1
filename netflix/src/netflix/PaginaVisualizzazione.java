package netflix;

import java.util.*;

import java.io.*;


import java.lang.NumberFormatException;
import java.util.InputMismatchException;

public class PaginaVisualizzazione  {
	Scanner scanner = new Scanner(System.in);
	//private int min;
	private int  cronologia;
	private long a=0;
    private long b=0;

	/** vettore di stringhe che specifica i possibili stati della Pagina di Visualizzazione
	 *
	*/
	private String[] stati= {"in attesa","mostrando scene","in pausa","chiusa"};
	private String stato;
	
	
	/** ritorna lo stato della pagina
	 * @return la string corrispondente allo stato
	 * @see netflix.PaginaVisualizzazione#stati
	*/
	public String getstato() {
		return stato;
	}

	/** costruttore di PaginaVisualizzazione
	 *<p>
	 *stampa messaggio a video
	 *<p>
	 *setto la stato della pagina "in attesa"
	 *@see netflix.PaginaVisualizzazione#stati
	*/
	PaginaVisualizzazione(){
		System.out.print("PaginaVisualizzazione istanziata \n");
		stato=stati[0];
	}
	
	
	/**  invocato dopo che viene inserito P 
	 *  <p>
	 *  viene avviata la riproduzione (quindi viene avviata la Pagina di Visualizzazione)
	* <p>
	* metodo che setta lo stato della pagina in riproduzione
	* @param c contenuto da avviare su cui verr� invocato metodo <code>preleva</code> {@link netflix.Contenuto#preleva()}
	* @param crono istanza di cronometro presente nel main, su cui verr� invocato metodo <code>startf</code>  {@link netflix.Cronometro#startf() }
	@see netflix.PaginaVisualizzazione#stati
	*/
	public void avvia(Contenuto c, Cronometro crono) {
		crono.startf();
		System.out.print("La riproduzione � stata avviata\n");
		stato=stati[1];
		c.preleva();
		System.out.println("Lo stato della pagina � "+ getstato());
	}
	

	/** invocato quando si vuole portare avanti il contenuto 
	 * <p>
	 * ferma il cronometro
	 * <p>
	 *  richiede di inserire i munuti se si supera la durata del contenuto 
	 *  <p>
	 *  avanza il cronometro
	 *  <p>
	 *  riavvia il cronometro
	 * @param c istanza di contenuto
	 * @param crono istanza di cronometro
	 * @exception InputMismatchException quando inseriamo una lettera o diverso da numero
	 @see netflix.Cronometro#stopf()
	 @see netflix.Cronometro#startf()
	 @see  netflix.Cronometro#ava(long a)
	*/
	public void avanti( Contenuto c, Cronometro crono) {
		
		 a=0;
		crono.stopf();
		boolean ok;
		do{
			ok=true;
			try {
				
				System.out.println("Inserisci i minuti: ");
			
		//String minuti = scanner.nextLine();
		
		System.out.print(crono.getime()+"\n");
		a= scanner.nextInt();
		
			}
			
catch (InputMismatchException e) {
	scanner.nextLine();
            System.out.println("Input non valido: ritenta...");
            ok=false;
	} 
			}while((crono.getime())+a>c.getdur() || a<0 || !ok);
		
			crono.ava(a);
			System.out.print(crono.getime()+"\n");
			crono.startf();
		//	c.skip(a);
			System.out.print("La riproduzione � stata portata avanti di "+a+" minuti \n"); 
		
		
	}
	
	/**
	 * 
	 * @return this.a di quanto � stato portato avanti il contenuto
	 */
	public long rita() {
		return this.a;
	}
	


	/**  invocato quando si vuole portare indietro il contenuto 
	 * <p>
	 * ferma il cronometro
	 * <p>
	 *  richiede di reinserire i munuti se si torna troppo indietro 
	 *  <p>
	 *  porta indietro il cronometro
	 *  <p>
	 *  fa ripartire il cronometro
	 * @param c istanza di contenuto
	 * @param crono istanza di cronometro
	 * @exception InputMismatchException quando inseriamo una lettera o diversi da numero
	  @see netflix.Cronometro#stopf()
	 @see netflix.Cronometro#startf()
	 @see  netflix.Cronometro#ind(long)
	*/
	
	public void indietro( Contenuto c, Cronometro crono) {
		crono.stopf();
		 b=0;
		boolean ok;
		do{
			ok=true;
			try {
				
				System.out.println("Inserisci i minuti: ");
			
		//String minuti = scanner.nextLine();
		
		System.out.print(crono.getime()+"\n");
		b= scanner.nextInt();
		
			}
			
catch (InputMismatchException e) {
	scanner.nextLine();
            System.out.println("Input non valido: ritenta...");
            ok=false;
	} 
			} while((crono.getime())-b<0 || b<0 || !ok);
		
		System.out.print(crono.getime()+"\n"); 
		
		//System.out.print(crono.getime()+"\n");
		crono.ind(b);
		crono.startf();
	//	c.back(b);
		System.out.print("La riproduzione � stata portata indietro di "+b+" minuti \n");
	}
	
	/**
	 * 
	 * @return this.b di quanto � stato portato indietro il contenuto
	 */
	public long ritd() {
		return this.b;
	}

	

	/** invocato quando si vuole mettere in pausa il contenuto ;
	 * <p>
	 * setta stato della pagina in pausa
	 * <p>
	 * ferma il contenuto
	 * @param c istanza di contenuto su cui verr� chiamato metodo ferma()
	 * @param crono istanza di cronometro su cui viene invocato metodo stopf()
	 *  @see netflix.Cronometro#stopf()
	 @see netflix.Contenuto#ferma()
	 
	
	*/
	
	public void pausa(Contenuto c, Cronometro crono) {
		crono.stopf();
		stato=stati[2];
		c.ferma();
		System.out.print("La riproduzione � in pausa\n");
		System.out.println("Lo stato della pagina � "+ getstato());
	}
	
	/** metodo invocato quando finisce  il contenuto 
	 * <p>
	 * setta stato della pagina in chiusa  {@link netflix.PaginaVisualizzazione#stati}
	 * @param c istanza di contenuto su cui verr� chiamato metodo fine()
	  @see netflix.Contenuto#finef()
	*/
	public void fine(Contenuto c) {
		c.finef();
		stato=stati[3];
		System.out.println("Lo stato della pagina � "+ getstato());
	}
	
	/** invocato quando si deve aggiornare (incrementare) la cronologia dopo la fine di un contenuto
	 * 
	*/
	public void aggiorna() {
			cronologia++;
			System.out.print("La cronologia � stata aggiornata\n");
		}
	
	/** 
	 * 
	 * @return numero elementi presenti in <code>cronologia</code>
	 */
	public int getCronologia () {
		return cronologia;
	}
	
	
	/**  invocato quando si vuole cambiare la lingua del contenuto ;
	 * 
	 * @param c istanza di contenuto su cui verr� chiamato metodo <code>enumIterate</code> {@link netflix.Contenuto#enumIterate()  }
	 *@exception InputMismatchException quando viene inserito qualcosa diverso da un numero
	 @see netflix.Contenuto#setlingua(String)
	 @see netflix.Contenuto#getlingua
	*/
	
	public void cambialingua(Contenuto c) {
		System.out.println("Quale lingua vuoi? \n");
		c.enumIterate();
		int numl=0;
		boolean ok;
		do{
			ok=true; 
			System.out.println("Inserisci numero corrispondente \n");
		try {
		numl= scanner.nextInt();
		
		}catch(InputMismatchException e) {
			scanner.nextLine();
			System.out.println("Comando non valido");
			ok=false;
		}
		
		
		}while ((numl<0 || numl>8) || !ok);
		
		
		switch(numl) {
	        case 1:
	        {   c.setlingua("italiano");
	            System.out.println("La lingua reimpostata � : ");
	            c.getlingua();
	            break;}
	        case 2:
	        {   c.setlingua("inglese");
            System.out.println("La lingua reimpostata � : ");
            c.getlingua();
            break;}
	       
	        case 3:
	        {   c.setlingua("francese");
            System.out.println("La lingua reimpostata � : ");
            c.getlingua();
            break;}
	        
	        case 4:
	        {   c.setlingua("spagnolo");
            System.out.println("La lingua reimpostata � : ");
            c.getlingua();
            break;}
	        
	        case 5:
	        {   c.setlingua("tedesco");
            System.out.println("La lingua reimpostata � : ");
            c.getlingua();
            break;}
	        
	        case 6:
	        {   c.setlingua("portoghese");
            System.out.println("La lingua reimpostata � : ");
            c.getlingua();
            break;}
	        
	        case 7:
	        {   c.setlingua("russo");
            System.out.println("La lingua reimpostata � : ");
            c.getlingua();
            break;}
		   
	        case 8: 
	        {   c.setlingua("cinese");
            System.out.println("La lingua reimpostata � : ");
            c.getlingua();
            break;}
		 }
	}
	
	/**  invocato quando si vogliono cambiare i sottotitoli del contenuto ;
	 * 
	 * @param c istanza di contenuto su cui verr� chiamato metodo <code>enumIterateS</code> {@link netflix.Contenuto#enumIterateS()  }
	 *@exception InputMismatchException quando viene inserito qualcosa diverso da un numero
	 @see netflix.Contenuto#setsotto(String)
	 @see netflix.Contenuto#getsotto
	*/
	
	
	public void cambiasotto(Contenuto c) {
		System.out.println("Quale lingua vuoi? \n");
		c.enumIterateS();
		int numl=0;
	
		boolean ok;
		do{
			ok=true; 
			System.out.println("Inserisci numero corrispondente \n");
		try {
		numl= scanner.nextInt();
		
		}catch(InputMismatchException e) {
			scanner.nextLine();
			System.out.println("Comando non valido");
			ok=false;
		}
		
		
		}while ((numl<0 || numl>9) || !ok);
		
		
		switch(numl) {
	        case 1:
	        {   c.setsotto("italiano");
	            System.out.println("La lingua dei sottotitoli reimpostata � : ");
	            c.getsotto();
	            break;}
	        case 2:
	        {   c.setsotto("inglese");
            System.out.println("La lingua dei sottotitoli reimpostata � : ");
            c.getlingua();
            break;}
	       
	        case 3:
	        {   c.setsotto("francese");
            System.out.println("La lingua dei sottotitoli reimpostata � : ");
            c.getsotto();
            break;}
	        
	        case 4:
	        {   c.setsotto("spagnolo");
            System.out.println("La lingua dei sottotitoli reimpostata � : ");
            c.getsotto();
            break;}
	        
	        case 5:
	        {   c.setsotto("tedesco");
            System.out.println("La lingua dei sottotitoli reimpostata � : ");
            c.getsotto();
            break;}
	        
	        case 6:
	        {   c.setsotto("portoghese");
            System.out.println("La lingua dei sottotitoli reimpostata � : ");
            c.getsotto();
            break;}
	        
	        case 7:
	        {   c.setsotto("russo");
            System.out.println("La lingua dei sottotitoli reimpostata � : ");
            c.getsotto();
            break;}
		   
	        case 8: 
	        {   c.setsotto("cinese");
            System.out.println("La lingua dei sottotitoli reimpostata � : ");
            c.getsotto();
            break;}
	        
	        case 9: 
	        {   c.setsotto("disattivati");
            System.out.println("I sottotitoli sono stati disattivati");
            
            break;}
		 }
	}
	
}