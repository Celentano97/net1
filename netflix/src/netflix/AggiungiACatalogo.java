package netflix;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import netflix.VisualizzaCatalogoFilm;


public class AggiungiACatalogo {
		
	//Delimiter used in CSV file
	private static final String COMMA_DELIMITER = ";";
	private static final String NEW_LINE_SEPARATOR = "\n";
		
	//static Genere genere;
	
	/** consente l'aggiunta di contenuti al catalogo
	 * 	
	 * @param fileName nome del csv in cui aggiungere contenuto
	 * @param scelta   assume valore F o S (dipende se voglio aggiungere film o serieTV)
	
	 */
	public static void AggiungiACatalogo(String fileName, String scelta) {
			
		String titolo, regista =null;
		int durata = 0;
		long nextId=0;
		int anno = 0;
		int stagione=0;
		int puntata = 0;
			long min=0;
		Scanner scanner = new Scanner(System.in);
		System.out.print("Inserisci il Titolo: \n");
		do {titolo = scanner.nextLine();	
			}while (titolo.equals(null)) ;
			
		System.out.print("Inserisci il Regista: \n");
		do {regista = scanner.nextLine();	
			}while (regista.equals(null)) ;
			
		System.out.print("Inserisci la Durata (compresa tra 1 minuto e 300 minuti): \n");
		do {durata = scanner.nextInt();	
			}while (durata<1 || durata>300);
			
			
		if (scelta.equals("F")) {
				System.out.print("Inserisci l'anno (compreso tra 1890 e 2020): \n");
				do {anno = scanner.nextInt();	
					}while (anno<1890 || anno>2020);
			
				System.out.println("Qual � il genere? \n");
				Film.enumIterate();
				int numl=0;
		
				boolean ok;
			
				do{
					ok=true; 
					System.out.println("\nInserisci numero corrispondente \n");
				try {
					numl= scanner.nextInt();
			
				}catch(InputMismatchException e) {
				scanner.nextLine();
				System.out.println("Comando non valido");
				ok=false;		
				}
				}while ((numl<0 || numl>14) || !ok);	
			
		//*****************SWITCH TRA VARIE SCELTE GENERE film ***************	
				
				switch(numl) {	
				
	   		    case 1:
				{   Film.setGen1("Animazione");
			 	Film.getGen1();  
				System.out.println("Il genere del film scelto � "+Film.genere);
				break;}
				
			    case 2:
			    {   Film.setGen1("Avventura");
			    Film.getGen1();  
			    System.out.println("Il genere del film scelto � "+Film.genere);
			    break;}
				
				case 3:
				{   Film.setGen1("Biografico");
				Film.getGen1();  
				System.out.println("Il genere del film scelto � "+Film.genere);
				break;}
				
				case 4:
				{   Film.setGen1("Commedia");
				Film.getGen1();  
				System.out.println("Il genere del film scelto � "+Film.genere);
				break;}
				
				case 5:
				{   Film.setGen1("Documentario");
				Film.getGen1();  
				System.out.println("Il genere del film scelto � "+Film.genere);
				break;}
				
				case 6:
				{   Film.setGen1("Drammatico");
				Film.getGen1();  
				System.out.println("Il genere del film scelto � "+Film.genere);
				break;}
				
				case 7:
				{   Film.setGen1("Fantascienza");
				Film.getGen1();  
				System.out.println("Il genere del film scelto � "+Film.genere);
				break;}
				
				case 8:
				{   Film.setGen1("Fantasy");
				Film.getGen1();  
				System.out.println("Il genere del film scelto � "+Film.genere);
				break;}
				
				case 9:
				{   Film.setGen1("Guerra");
				Film.getGen1();  
				System.out.println("Il genere del film scelto � "+Film.genere);
				break;}
				
				case 10:
				{   Film.setGen1("Horror");
				Film.getGen1();  
				System.out.println("Il genere del film scelto � "+Film.genere);
				break;}
				
				case 11:
				{   Film.setGen1("Musical");
				Film.getGen1();  
				System.out.println("Il genere del film scelto � "+Film.genere);
				break;}
				
				case 12:
				{   Film.setGen1("Storico");
				Film.getGen1();  
				System.out.println("Il genere del film scelto � "+Film.genere);
				break;}
				
				case 13:
				{   Film.setGen1("Thriller");
				Film.getGen1();  
				System.out.println("Il genere del film scelto � "+Film.genere);
				break;}
				
				case 14:
				{   Film.setGen1("Western");
				Film.getGen1();  
				System.out.println("Il genere del film scelto � "+Film.genere);
				break;}
				}
				
		//*****************FINE SWITCH TRA VARIE SCELTE GENERE film ***************	
				}  //chiude if F (amministratore aggiunge film)
		
		
		else {
				System.out.print("Inserisci la stagione: \n");
				do {stagione = scanner.nextInt();	
					}while (stagione==0);
			
				System.out.print("Inserisci la puntata: \n");
				do {puntata = scanner.nextInt();	
				}while (puntata==0);
			
			  }
			
	// ****************** CALCOLO DELL'ID DA USARE NEL CATALOGO *****************
			BufferedReader fileReader = null;
		    
		    try {
		    	
		       	
		        String line = "";
		        
		        //Creazione di un File Reader
		        fileReader = new BufferedReader(new FileReader(fileName));
		        
		        //Read the CSV file header to skip it
		       //fileReader.readLine();
		        
		        //Read the file line by line starting from the second line
		        while ((line = fileReader.readLine()) != null) {
		          nextId++;
		        }
		        nextId=nextId+1;
		        
		    	} 
		    	catch (Exception e) {
		    		System.out.println("Errore nel CSV!!!");
		    		e.printStackTrace();
		    	} finally {
		    		
		        try {
		            fileReader.close();
		            
		        } catch (IOException e) {
		        	System.out.println("Errore mentre il CSV veniva chiuso!!!");
		            e.printStackTrace();
		        }
		    	}    
		 // ****************** FINE CALCOLO DELL'ID DA USARE NEL CATALOGO *****************
	
		    
	//Creazione nuovi oggetti contenuto
			
	          
			
		    FileWriter fileWriter = null;
					
		    try {
		    	fileWriter = new FileWriter(fileName,true); 
						
		    	if (scelta.equals("F")) {
				Film contenuto1 = new Film(nextId, titolo, regista, durata,min, anno, Film.genere);
			       
				
				//Creazione nuova lista di oggetti contenuto
				List<Film> contenutiF = new ArrayList<Film>();
				contenutiF.add(contenuto1);
				
				fileWriter.append(String.valueOf(contenuto1.getId()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto1.getTitolo()));
				fileWriter.append(COMMA_DELIMITER);					
				fileWriter.append(String.valueOf(contenuto1.getRegista()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto1.getdur()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto1.getmin()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto1.getAnno()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(Film.getGen1()));
				fileWriter.append(NEW_LINE_SEPARATOR);
			
				System.out.println("Il nuovo contenuto � stato aggiunto con successo !!!");
				System.out.println("Hai aggiunto il contenuto " + " id=" + nextId + ", titolo=" + titolo
					+ ", regista=" + regista + ", durata=" + durata + ", anno=" + anno + ", genere = " + Film.genere);
				}
			
			else {
				Serie contenuto2 = new Serie(nextId, titolo, regista, durata, min, stagione,puntata);
				List<Serie> contenutiS = new ArrayList<Serie>();
				contenutiS.add(contenuto2);
				
				fileWriter.append(String.valueOf(contenuto2.getId()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto2.getTitolo()));
				fileWriter.append(COMMA_DELIMITER);					
				fileWriter.append(String.valueOf(contenuto2.getRegista()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto2.getdur()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto2.getmin()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto2.getstagione1()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto2.getpuntata1()));
				fileWriter.append(NEW_LINE_SEPARATOR);
				
				
				System.out.println("Il nuovo contenuto � stato aggiunto con successo !!!");
				System.out.println("Hai aggiunto il contenuto " + "id=" + nextId + ", titolo=" + titolo
						+ ", regista=" + regista + ", durata=" + durata + ", stagione =" + stagione+ ", puntata = " + puntata);
				}			
				
				
			} catch (Exception e) {
				System.out.println("Errore nella scrittura del CSV !!!");
				e.printStackTrace();
			} finally {
				
				try {
					fileWriter.flush();
					fileWriter.close();
					
				} catch (IOException e) {
					System.out.println("Errore durante la chiusura/flushing della scrittura del file !!!");
	                e.printStackTrace();
				}				
			}
		}
	
	public static void RimuoviCatalogo(String fileName, String scelta) {
		Scanner scanner = new Scanner(System.in);
		int ni=0;
		//System.out.print("Quale vuoi eliminare: \n");
		
		if (scelta.equals("F")) {
			
			VisualizzaCatalogoFilm.VisualizzaCatalogoFilm(fileName);
			ArrayList<Film> af=new ArrayList<Film>();
			af=VisualizzaCatalogoFilm.arr();
			int nd=0;
			do {
			System.out.print("Quale vuoi eliminare (inserisci ID): \n");
			nd=scanner.nextInt();
			} while(nd<1 || nd>af.size());
		    
			af.remove(nd);
		   System.out.println("Il film � stato eliminato");
		   VisualizzaCatalogoFilm.VisualizzaCatalogoFilm(fileName);
		}
			
		else if (scelta.equals("S")) {
			VisualizzaCatalogoSerie.VisualizzaCatalogoSerie(fileName);
			ArrayList<Serie> as=new ArrayList<Serie>();
			as=VisualizzaCatalogoSerie.arr();
			int nd=0;
			do {
			System.out.print("Quale vuoi eliminare (inserisci ID): \n");
			nd=scanner.nextInt();
			} while(nd<1 || nd>as.size());
		
			as.remove(nd);
			  System.out.println("La serie � stata eliminata");
			  VisualizzaCatalogoSerie.VisualizzaCatalogoSerie(fileName);
		}
		
	}

	
	
}