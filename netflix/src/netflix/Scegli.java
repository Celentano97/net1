package netflix;


import java.util.ArrayList;
import java.util.InputMismatchException;
import netflix.VisualizzaCatalogo;
import netflix.Contenuto;
import netflix.VisualizzaCatalogoSerie;
import netflix.Serie;
import netflix.VisualizzaCatalogoFilm;
import netflix.Film;
import java.io.*;
import java.util.*;



public class Scegli {

	private static Contenuto c;
	
	/**Fornisce il contenuto scelto
	 * 
	 * @return c contenuto scelto
	 */
	public static Contenuto ritc() {
		return c;
	}
	

	/**Implementa caso d'uso ScegliContenuto
	 * <p>
	 * consente di scegliere tra <code>Film</code> e <code>Serie</code>
	 * @see netflix.VisualizzaCatalogoSerie#VisualizzaCatalogoSerie(String)
	 * @see netflix.VisualizzaCatalogoFilm#VisualizzaCatalogoFilm(String)
	 * @see netflix.VisualizzaCatalogoSerie#arr()
	 * @see netflix.VisualizzaCatalogoFilm#arr()
	 */
	public static void Scegli() {
	 Scanner scanner= new Scanner(System.in);	
		
		int fs=0;
		boolean ok=true;
		
	do {
		ok=true;
		try {
	
			System.out.println("Scegli tra Film (1) o SerieTV (2)");
			fs=scanner.nextInt();
		
	} catch (InputMismatchException e) {
		System.out.println("Non corretto");
		scanner.nextLine();
		ok=false;
	}
	}while((fs!=1 && fs!=2) || !ok);
	
	Film f=null;
	Serie s=null;
	
	if (fs==2) {
		String fileNames = "cats.csv";
		//String fileNames = "cats.csv";
		VisualizzaCatalogoSerie.VisualizzaCatalogoSerie(fileNames); 
		ArrayList<Serie> se=VisualizzaCatalogoSerie.arr();
		int numse=se.size();
		System.out.println("Dimensione ArrayList= "+ numse );
		int nums=0;
		boolean okki=true;
		do {
			okki=true;
			try {
		
				System.out.println("Quale vuoi riprodurre? (inserisci numero da 1 a " + numse +")" );
				nums=scanner.nextInt();
			    
				
		} catch (InputMismatchException e) {
			System.out.println("Non corretto");
			scanner.nextLine();
			okki=false;
		}
		}while(((nums-1)<0 || (nums-1)>(numse-1)) || !okki);
		
		s=se.get(nums-1);	  	
		}
	
	
	else if(fs==1) {
		String fileNamef = "catf.csv";
		VisualizzaCatalogoFilm.VisualizzaCatalogoFilm(fileNamef);
		ArrayList<Film> fi=VisualizzaCatalogoFilm.arr();
		int numfi=fi.size();
		System.out.println("\nDimensione ArrayList= "+ numfi + "\n");
		 int num=0;
		boolean oki=true;
		
		do {
			oki=true;
			try {
		
				System.out.println("Quale vuoi riprodurre? (inserisci numero da 1 a " + numfi +")\n" );
				num=scanner.nextInt();
			    
				
		} catch (InputMismatchException e) {
			System.out.println("Non corretto");
			scanner.nextLine();
			oki=false;
		}
		}while(((num-1)<0 || (num-1)>(numfi-1)) || !oki);
		
		f=fi.get(num-1);	  
	}
	
	if(fs==1) {
	System.out.println("Il contenuto in riproduzione �: "+ f.toString());
	c=f;
	}
	else {
		System.out.println("Il contenuto in riproduzione �: "+ s.toString());
	c=s;
	}
			
	}
}