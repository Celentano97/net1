package netflix;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;


import netflix.Film.Genere;



public class AggiungiInterrotti {
		
	//Delimiter used in CSV file
	private static final String COMMA_DELIMITER = ";";
	private static final String NEW_LINE_SEPARATOR = "\n";
	private static final int CONTENUTI_ID_IDX = 0;
	private static final int CONTENUTI_TITOLO_IDX = 1;
	private static final int CONTENUTI_AUTORE_IDX = 2;
	private static final int CONTENUTI_DURATA = 3; 
	private static final int CONTENUTI_MIN_IDX=4;
	//static Genere genere;
	private String filename;
	

	/**Costruttore della classe AggiungiInterrotti
	 * 
	 * @param filename nome del file csv
	 */
	AggiungiInterrotti(String filename) {
		this.filename=filename;
		
	}
	
	/**Aggiunge ad un file i contenuti che non sono stati riprodotti per intero
	 * 
	 * @param fileName  nome file dove sono presenti i contenuti interrotti
	 * @param c Contenuto da aggiungere al file
	 */
	public static void Aggiungi(String fileName, Contenuto c) {
		
			
		//Scanner scanner = new Scanner(System.in);

		    FileWriter fileWriter = null;
		    
		    Contenuto contenuto1=c;
		    ArrayList contenutiF = new ArrayList();
		    
		  // ArrayList<Contenuto> carica=new ArrayList<Contenuto>();
		    //carica=AggiungiCronologia.arrc();
		    
		     	contenutiF.add(contenuto1);
		          
			
			//inserisce nuovo contenuto alla cronologia
		    try {
		    	fileWriter = new FileWriter(fileName,true); 
						
				
                
				
				fileWriter.append(String.valueOf(contenuto1.getId()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto1.getTitolo()));
				fileWriter.append(COMMA_DELIMITER);					
				fileWriter.append(String.valueOf(contenuto1.getRegista()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto1.getdur()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(contenuto1.getmin()));
				fileWriter.append(COMMA_DELIMITER);
				//fileWriter.append(String.valueOf(contenuto1.getAnno()));
				//fileWriter.append(COMMA_DELIMITER);
				//fileWriter.append(String.valueOf(Film.getGen1()));
				
				
				fileWriter.append(NEW_LINE_SEPARATOR); //QUESTO
			
				System.out.println("Il nuovo contenuto � stato aggiunto con successo in interrotti !!!");
				System.out.println("E' stato aggiungo il contenuto " + contenuto1 + " alla lista interrotti");
				
			
				
				
			} catch (Exception e) {
				System.out.println("Errore nella scrittura del CSV !!!");
				e.printStackTrace();
			} finally {
				
				try {
					fileWriter.flush();
					fileWriter.close();
					
				} catch (IOException e) {
					System.out.println("Errore durante la chiusura/flushing della scrittura del file !!!");
	                e.printStackTrace();
				}				
			}
		    }
		
private static ArrayList<Contenuto>  contenutii;  //
	
/**Consente di ottenere l'array di contenuti presente nel file csv
 * 
 * @return <code>contenutii</code> l'array di contenuti 
 */
	public static  ArrayList<Contenuto> arr (){
		return contenutii;
	}
	
	/** Mostra i contenuti che sono stati interrotti 
	 
	 * @param fileName nome del file dove ci sono i contenuti interrotti
	 */
	public static void VisualizzaInterrotti (String fileName) {
		
		{

			BufferedReader fileReader = null;
	     
	        try {
	        	
	        	//Crea una nuova lista di contenuto che deve essere inserita in un file CSV 
	        	 contenutii = new ArrayList<Contenuto>();
	        	
	            String line = "";
	            
	            //Creazione di un File Reader
	            fileReader = new BufferedReader(new FileReader(fileName));
	            
	            //Read the CSV file header to skip it
	           //fileReader.readLine();
	            
	            //Read the file line by line starting from the second line
	            while ((line = fileReader.readLine()) != null) {
	                //Get all tokens available in line
 	                String[] tokens = line.split(COMMA_DELIMITER);
	                if (tokens.length > 0) {
	                	//Creazione di un nuovo oggetto contenuto e aggiunta degli attributi
	                	Contenuto contenutic = new Contenuto(Long.parseLong(tokens[CONTENUTI_ID_IDX]), tokens[CONTENUTI_TITOLO_IDX], tokens[CONTENUTI_AUTORE_IDX], Integer.parseInt (tokens[CONTENUTI_DURATA]), Long.parseLong (tokens[CONTENUTI_MIN_IDX]));
						contenutii.add(contenutic);
					}
	            }
	            
	            //Stampa della lista dei contenuti 
	            for (Contenuto contenutic : contenutii) {
					System.out.println(contenutic.toString());
				}
	            
	            System.out.println("Il numero di contenuti interrotti � : "+ contenutii.size());
	        } 
	        catch (Exception e) {
	        	System.out.println("Errore nel CSV!!!");
	            e.printStackTrace();
	        } finally {
	            try {
	                fileReader.close();
	            } catch (IOException e) {
	            	System.out.println("Errore mentre il CSV veniva chiuso!!!");
	                e.printStackTrace();
	            }
	        }

		}
	  
	}
	
	
	}