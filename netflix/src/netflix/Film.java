package netflix;

/**<code>Film</code> classe derivata da <code>Contenuto</code>
 *
 */
public class Film extends Contenuto{
	
	static Genere genere;
	protected long anno=0;
	
	/** costruttore di Film
	 * @param id identificativo del Film 
	 * @param titolo titolo del Film 
	 * @param autore regista del Film
	 * @param durata durata del Film 
	 * @param min minuto cui � stato eventualmente interrotto il Film
	 * @param anno anno di uscita del Film
	 * @param gen genere del Film 
	 */
	public Film (long id, String titolo, String autore, long durata,long min, long anno, Genere gen) {
		super(id,titolo,autore,durata,min);
		
		//System.out.println("Entity Film istanziata");
		this.anno=anno;
		this.genere=gen;
	}
	
	
	
	public  String toString() {
		return " [id=" + id + ", titolo=" + titolo
				+ ", regista=" + regista + ", durata=" + durata + ", anno=" + anno + ", genere = " + Film.genere +",minuto= " +min+"]";
	}
	
	
	
	
	/** lista dei generi a cui il Film pu� appartenere
	 *
	 */
	public enum Genere{
		Animazione,
		Avventura,
		Biografico,
		Commedia,
		Documentario,
		Drammatico,
		Fantascienza,
		Fantasy,
		Guerra,
		Horror,
		Musical,
		Storico,
		Thriller,
		Western;	
	}
	
	/** stampa a video l'elenco dei generi
	 * 
	 */
	public static void enumIterate() {
	    int j=1;
		for ( Genere  so: Genere.values()) {
	        System.out.println(j+" "+so.name());
	        j=j+1;
	    }
	}
	
	/** setta il genere del Film che si vuole vedere
	 * 
	 * @param g stringa che indica il genere
	 */
	public void setGen(String g) {
		Film.genere=Genere.valueOf(g);
	}
	
	/** stampa a video il genere scelto
	 * 
	 */
	public void getGen() {
		System.out.println("Il genere del film scelto  "+Film.genere);
			}
	
	/** setta il genere del Film che si vuole vedere
	 * 
	 * @param g stringa che indica il genere
	 */
	public static void setGen1(String g) {
		Film.genere=Genere.valueOf(g);
	}
	
	/**Fornisce il genere del film
	 * 
	 * @return Film.genere genere del film
	 */
	public static Genere getGen1() {
		return Film.genere;
	}
	
	/**Fornisce anno in cui � stato prodotto il film
	 * 
	 * @return anno deil film
	 */
	public long getAnno() {
		return anno;
	}
	
}