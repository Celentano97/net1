package netflix;

public class Buffer {

	String s="";
	long tp=0;
	Buffer (){
		
	}
	
	/**Funzione che setta il tempo avanti o indietro
	 * <p>
	 * settato da RiproThread
	 * 
	 * @param tp tempo avanti o indietro del contenuto
	 */
	public synchronized void setTP(long tp) {
		this.tp=tp;
	}
	
	/**Fornsice il tempo portato avanti o indietro
	 * <p>
	 * Letto da CronoThread
	 * @return this.tp tempo
	 */
	public synchronized long getTP() {
		return this.tp;
	}
	
	/**Fornisce stringa che indica eventuale Pausa
	 * <p>
	 * Letto da CronoThread
	 * 
	 * @return this.s stringa
	 */
	public synchronized String getString() {
		return this.s;
	};
	
	/**Settato a <code>p</code> da RiproServer quando si mette PAUSA
	 * <p>
	 * @param s stringa che indica eventuale PAUSA
	 */
	public synchronized void  setString(String s) {
		this.s=s;
	}
}
